package it.generali.eai.broker.bean;


import java.io.Serializable;

public class OggettoPortaleProfessionista implements Serializable{

	private static final long serialVersionUID = 1L;
	private String compProf;
	private String codProf;
	private String idIncarico;
	private String compProfOriginale;
	private String codProfOriginale;
	private String idIncaricoOriginale;
	

	public String getCompProf() {
		return compProf;
	}

	public void setCompProf(String compProf) {
		this.compProf = compProf;
	}

	public String getCodProf() {
		return codProf;
	}

	public void setCodProf(String codProf) {
		this.codProf = codProf;
	}

	public String getIdIncarico() {
		return idIncarico;
	}

	public void setIdIncarico(String idIncarico) {
		this.idIncarico = idIncarico;
	}

	public String getCompProfOriginale() {
		return compProfOriginale;
	}

	public void setCompProfOriginale(String compProfOriginale) {
		this.compProfOriginale = compProfOriginale;
	}

	public String getCodProfOriginale() {
		return codProfOriginale;
	}

	public void setCodProfOriginale(String codProfOriginale) {
		this.codProfOriginale = codProfOriginale;
	}

	public String getIdIncaricoOriginale() {
		return idIncaricoOriginale;
	}

	public void setIdIncaricoOriginale(String idIncaricoOriginale) {
		this.idIncaricoOriginale = idIncaricoOriginale;
	}

}
