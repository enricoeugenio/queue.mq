package it.generali.eai.broker.bean;

import java.io.Serializable;

public class OffertaPortaleProfessionista implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Attributi necessari per reperire l’offerta in A.O.L.
	private String autoOnlinePackageID;
	private String localPackageID;

	// Attributi necessari per inserire il documento di offerta nel documentale
	private String flussoDocumentale;
	private String numeroIncaricoProfessionista;
	private String codiceAgenzia;
	private String codiceCompagnia;
	private String estensioneFile;
	private String codiceProgressivoSinistro;
	private String tipoDocumento;
	private String idDocumento;
	private String sorgente;
	private String codiceNominativo;
	private String annoEsercizioDiRubricazione;
	private String idUtenteUtilizzatore;

	public String getAutoOnlinePackageID() {
		return autoOnlinePackageID;
	}

	public void setAutoOnlinePackageID(String autoOnlinePackageID) {
		this.autoOnlinePackageID = autoOnlinePackageID;
	}

	public String getLocalPackageID() {
		return localPackageID;
	}

	public void setLocalPackageID(String localPackageID) {
		this.localPackageID = localPackageID;
	}

	public String getFlussoDocumentale() {
		return flussoDocumentale;
	}

	public void setFlussoDocumentale(String flussoDocumentale) {
		this.flussoDocumentale = flussoDocumentale;
	}

	public String getNumeroIncaricoProfessionista() {
		return numeroIncaricoProfessionista;
	}

	public void setNumeroIncaricoProfessionista(String numeroIncaricoProfessionista) {
		this.numeroIncaricoProfessionista = numeroIncaricoProfessionista;
	}

	public String getCodiceAgenzia() {
		return codiceAgenzia;
	}

	public void setCodiceAgenzia(String codiceAgenzia) {
		this.codiceAgenzia = codiceAgenzia;
	}

	public String getCodiceCompagnia() {
		return codiceCompagnia;
	}

	public void setCodiceCompagnia(String codiceCompagnia) {
		this.codiceCompagnia = codiceCompagnia;
	}

	public String getEstensioneFile() {
		return estensioneFile;
	}

	public void setEstensioneFile(String estensioneFile) {
		this.estensioneFile = estensioneFile;
	}

	public String getCodiceProgressivoSinistro() {
		return codiceProgressivoSinistro;
	}

	public void setCodiceProgressivoSinistro(String codiceProgressivoSinistro) {
		this.codiceProgressivoSinistro = codiceProgressivoSinistro;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getSorgente() {
		return sorgente;
	}

	public void setSorgente(String sorgente) {
		this.sorgente = sorgente;
	}

	public String getCodiceNominativo() {
		return codiceNominativo;
	}

	public void setCodiceNominativo(String codiceNominativo) {
		this.codiceNominativo = codiceNominativo;
	}

	public String getAnnoEsercizioDiRubricazione() {
		return annoEsercizioDiRubricazione;
	}

	public void setAnnoEsercizioDiRubricazione(String nnoEsercizioDiRubricazione) {
		this.annoEsercizioDiRubricazione = nnoEsercizioDiRubricazione;
	}

	public String getIdUtenteUtilizzatore() {
		return idUtenteUtilizzatore;
	}

	public void setIdUtenteUtilizzatore(String idUtenteUtilizzatore) {
		this.idUtenteUtilizzatore = idUtenteUtilizzatore;
	}

}
