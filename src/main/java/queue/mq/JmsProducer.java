package queue.mq;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;

public class JmsProducer {

	private static final String BROKER_ADDRESS = "tcp://localhost:61616";
	private static final String PTF_SOA_NOTIFICA_ASCII = "PTF.SOA.NOTIFICA_ASCII";
	private static Map<String, JMSConfig> configurations;
	private static volatile Thread brokerThread = null;
	private static volatile boolean brokerThreadRunning = true;

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {

		configurations = new HashMap<String, JMSConfig>();

		thread(new Producer(PTF_SOA_NOTIFICA_ASCII, "broker:(" + BROKER_ADDRESS + ")", BROKER_ADDRESS), false);

		Scanner scanner = null;
		String input = "";

		synchronized (input) {

			do {
				System.out.println("(" + BrokerMessageCreator.SISTEMA_DESTINAZIONE.IOT.getName()
						+ "): send message to queue CODA_NOTIFICA_ASCII with an IOT message");
				System.out.println("(" + BrokerMessageCreator.SISTEMA_DESTINAZIONE.VIDEOPERIZIA.getName()
						+ "): send message to queue CODA_NOTIFICA_ASCII with a VIDEOPERZIA message");
				System.out.println("(" + BrokerMessageCreator.SISTEMA_DESTINAZIONE.ASV.getName()
						+ "): send message to queue CODA_NOTIFICA_ASCII with an ASV message");
				System.out.println("(C): close connection");
				scanner = new Scanner(System.in);
				input = scanner.nextLine();
				if (BrokerMessageCreator.SISTEMA_DESTINAZIONE.IOT.getName().equalsIgnoreCase(input)) {
					write(input);
				}
			} while (!"C".equalsIgnoreCase(input));

			System.out.println();

			close();
		}

	}

	public static void thread(Runnable runnable, boolean daemon) {
		brokerThread = new Thread(runnable);
		brokerThread.setDaemon(daemon);
		brokerThread.start();
	}

	public static void write(String input) {

		BrokerMessageCreator brokerMessageCreator = new BrokerMessageCreator();

		JMSConfig jmsConfig;
		TextMessage message;
		try {
			if (BrokerMessageCreator.SISTEMA_DESTINAZIONE.IOT.getName().equalsIgnoreCase(input)) {
				System.out.println("\n\n Writing " + BrokerMessageCreator.SISTEMA_DESTINAZIONE.IOT.getName() + " message in queue... " );
				jmsConfig = configurations.get(PTF_SOA_NOTIFICA_ASCII);
				message = jmsConfig.getSession().createTextMessage(
						brokerMessageCreator.getMessage(BrokerMessageCreator.SISTEMA_DESTINAZIONE.IOT));
				jmsConfig.getMessageProducer().setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				System.out.println("...");
				jmsConfig.getMessageProducer().send(message);
				System.out.println("SENT!");
			} else if (BrokerMessageCreator.SISTEMA_DESTINAZIONE.ASV.getName().equalsIgnoreCase(input)) {
				System.out.println("\n\n Writing " + BrokerMessageCreator.SISTEMA_DESTINAZIONE.ASV.getName() + " message in queue... " );
				jmsConfig = configurations.get(PTF_SOA_NOTIFICA_ASCII);
				message = jmsConfig.getSession().createTextMessage(
						brokerMessageCreator.getMessage(BrokerMessageCreator.SISTEMA_DESTINAZIONE.ASV));
				jmsConfig.getMessageProducer().setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				System.out.println("...");
				jmsConfig.getMessageProducer().send(message);
				System.out.println("SENT!");
			} else if (BrokerMessageCreator.SISTEMA_DESTINAZIONE.VIDEOPERIZIA.getName().equalsIgnoreCase(input)) {
				System.out.println("\n\n Writing " + BrokerMessageCreator.SISTEMA_DESTINAZIONE.VIDEOPERIZIA.getName() + " message in queue... " );
				jmsConfig = configurations.get(PTF_SOA_NOTIFICA_ASCII);
				message = jmsConfig.getSession().createTextMessage(
						brokerMessageCreator.getMessage(BrokerMessageCreator.SISTEMA_DESTINAZIONE.VIDEOPERIZIA));
				jmsConfig.getMessageProducer().setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				System.out.println("...");
				jmsConfig.getMessageProducer().send(message);
				System.out.println("SENT!");
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public static void close() {
		try {
			for (Map.Entry<String, JMSConfig> entry : configurations.entrySet()) {
				entry.getValue().getSession().close();
				entry.getValue().getConnection().close();
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

		brokerThreadRunning = false;
		brokerThread.setDaemon(brokerThreadRunning);
		System.out.println("BYE!");
		System.exit(0);
	}

	private static class Producer implements Runnable {

		private JMSConfig jmsConfig;
		private String queueName;

		public Producer(String queueName, String brokerURI, String brokerURL) {
			this.queueName = queueName;
			try {
				jmsConfig = new JMSConfig(brokerURI);
				jmsConfig.setActiveMQConnectionFactory(new ActiveMQConnectionFactory(brokerURL));
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}

		public void run() {

			try {
				jmsConfig.setBroker(BrokerFactory.createBroker(jmsConfig.getBrokerURI()));
				jmsConfig.getBroker().setBrokerName("BROKER_" + queueName);
				jmsConfig.getBroker().start();
			} catch (URISyntaxException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
				System.exit(1);
			} catch (Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
				System.exit(1);
			}

			try {
				jmsConfig.setConnection(
						jmsConfig.getActiveMQConnectionFactory().createConnection("userName", "password"));
				jmsConfig.getConnection().start();
				jmsConfig.setSession(jmsConfig.getConnection().createSession(false, Session.AUTO_ACKNOWLEDGE));
				jmsConfig.setMessageProducer(
						jmsConfig.getSession().createProducer(jmsConfig.getSession().createQueue(queueName)));
				configurations.put(queueName, jmsConfig);
			} catch (Exception e) {
				System.out.println("Caught: " + e);
				e.printStackTrace();
			}
		}

	}

	public static class JMSConfig {
		private ActiveMQConnectionFactory activeMQConnectionFactory;
		private Connection connection;
		private BrokerService broker;
		private Session session;
		private URI brokerURI;
		private MessageProducer messageProducer;

		public JMSConfig() {
		};

		public JMSConfig(String brokerURI) throws URISyntaxException {
			this.brokerURI = new URI(brokerURI);
		};

		public ActiveMQConnectionFactory getActiveMQConnectionFactory() {
			return activeMQConnectionFactory;
		}

		public void setActiveMQConnectionFactory(ActiveMQConnectionFactory activeMQConnectionFactory) {
			this.activeMQConnectionFactory = activeMQConnectionFactory;
		}

		public Connection getConnection() {
			return connection;
		}

		public void setConnection(Connection connection) {
			this.connection = connection;
		}

		public Session getSession() {
			return session;
		}

		public void setSession(Session session) {
			this.session = session;
		}

		public BrokerService getBroker() {
			return broker;
		}

		public void setBroker(BrokerService broker) {
			this.broker = broker;
		}

		public URI getBrokerURI() {
			return brokerURI;
		}

		public void setBrokerURI(URI brokerURI) {
			this.brokerURI = brokerURI;
		}

		public MessageProducer getMessageProducer() {
			return messageProducer;
		}

		public void setMessageProducer(MessageProducer messageProducer) {
			this.messageProducer = messageProducer;
		}

	}

}
