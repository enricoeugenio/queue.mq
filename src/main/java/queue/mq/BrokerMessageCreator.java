package queue.mq;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class BrokerMessageCreator {

	private List<Couple<String, Integer>> list;

	public enum SISTEMA_DESTINAZIONE {
		IOT("IOT"), VIDEOPERIZIA("VIP"), ASV("ASV");

		private String name;

		SISTEMA_DESTINAZIONE(String name) {
			this.setName(name);
		}

		public String getName() {
			return name;
		}

		private void setName(String name) {
			this.name = name;
		}

	}

	public String getMessage(SISTEMA_DESTINAZIONE sistemaDestinazione) {

		switch (sistemaDestinazione) {
		case IOT:
			return createMessageIOT();
		case VIDEOPERIZIA:
			return createMessageVideoperizia();
		case ASV:
			return createMessageASV();
		}
		return null;

	}

	public BrokerMessageCreator() {
		list = new ArrayList<Couple<String, Integer>>();
		initCopy();
	}

	private void initCopy() {
		list.add(new Couple<String, Integer>("HEADER-ABI", 1000));
		list.add(new Couple<String, Integer>("ID-NOTIFICA", 9));
		list.add(new Couple<String, Integer>("SISTEMA-PROVENIENZA", 3));
		list.add(new Couple<String, Integer>("CODICE-FUNZIONALITA", 10));
		list.add(new Couple<String, Integer>("TIPO-NOTIFICA", 1));
		list.add(new Couple<String, Integer>("DURATA", 3));
		list.add(new Couple<String, Integer>("CODICE-NODO", 5));
		list.add(new Couple<String, Integer>("CODICE-COLLABORATORE", 20));
		list.add(new Couple<String, Integer>("CODICE-ASSOCIAZIONE", 6));
		list.add(new Couple<String, Integer>("DATA-INSERIMENTO", 10));
		list.add(new Couple<String, Integer>("MESSAGGIO", 4000));
		for (int occurs = 0; occurs < 100; occurs++) {
			list.add(new Couple<String, Integer>("CHIAVE-" + occurs, 10));
			list.add(new Couple<String, Integer>("VALORE-" + occurs, 200));
		}
	}

	class Couple<K, V> {
		private String key;
		private Integer value;

		public Couple(String key, Integer value) {
			this.setKey(key);
			this.setValue(value);
		}

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}
	}

	private String createMessageIOT() {
		StringBuilder sb = new StringBuilder();
		for (Couple<String, Integer> couple : list) {
			switch (couple.getKey()) {
			case "HEADER-ABI":
				sb.append(createField(couple, ""));
				break;
			case "ID-NOTIFICA":
				sb.append(createField(couple, "123456"));
				break;
			case "SISTEMA-PROVENIENZA":
				sb.append(createField(couple, "IOT"));
				break;
			case "CODICE-FUNZIONALITA":
				sb.append(createField(couple, "IOT_PUSH"));
				break;
			case "TIPO-NOTIFICA":
				sb.append(createField(couple, "I"));
				break;
			case "DURATA":
				sb.append(createField(couple, "1"));
				break;
			case "CODICE-NODO":
				sb.append(createField(couple, ""));
				break;
			case "CODICE-COLLABORATORE":
				sb.append(createField(couple, ""));
				break;
			case "CODICE-ASSOCIAZIONE":
				sb.append(createField(couple, ""));
				break;
			case "DATA-INSERIMENTO":
				sb.append(createField(couple, "2018-10-29"));
				break;
			case "MESSAGGIO":
				sb.append(createField(couple, ""));
				break;
			case "CHIAVE-0":
				sb.append(createField(couple, "CODSOGG"));
				break;
			case "VALORE-0":
				sb.append(createField(couple, "2346654"));
				break;
			case "CHIAVE-1":
				sb.append(createField(couple, "CODEMAIL"));
				break;
			case "VALORE-1":
				sb.append(createField(couple, "999-001"));
				break;
			case "CHIAVE-2":
				sb.append(createField(couple, "CODCELL"));
				break;
			case "VALORE-2":
				sb.append(createField(couple, "999-007"));
				break;
			case "CHIAVE-3":
				sb.append(createField(couple, "POLIZZA"));
				break;
			case "VALORE-3":
				sb.append(createField(couple, "275185232"));
				break;
			case "CHIAVE-4":
				sb.append(createField(couple, "IOTVOUCHER"));
				break;
			case "VALORE-4":
				sb.append(createField(couple, "3200060490"));
				break;
			case "CHIAVE-5":
				sb.append(createField(couple, "IOTPROVIDR"));
				break;
			case "VALORE-5":
				sb.append(createField(couple, "VIASAT"));
				break;
			case "CHIAVE-6":
				sb.append(createField(couple, "NOMEAREA"));
				break;
			case "VALORE-6":
				sb.append(createField(couple, ""));
				break;
			case "CHIAVE-7":
				sb.append(createField(couple, "IDAREA"));
				break;
			case "VALORE-7":
				sb.append(createField(couple, "da480739-d488-4eec-b0c0-da0699cfa218"));
				break;
			case "CHIAVE-8":
				sb.append(createField(couple, "TIPOALERT"));
				break;
			case "VALORE-8":
				sb.append(createField(couple, "ENTRANCE"));
				break;
			case "CHIAVE-9":
				sb.append(createField(couple, "PROSSIMITA"));
				break;
			case "VALORE-9":
				sb.append(createField(couple, "FALSE"));
				break;
			default:
				sb.append(createField(couple, ""));
				break;
			}
		}
		return sb.toString();
	}

	private String createMessageVideoperizia() {
		StringBuilder sb = new StringBuilder();
		for (Couple<String, Integer> couple : list) {
			switch (couple.getKey()) {
			case "HEADER-ABI":
				sb.append(createField(couple, ""));
				break;
			case "ID-NOTIFICA":
				sb.append(createField(couple, "12138"));
				break;
			case "SISTEMA-PROVENIENZA":
				sb.append(createField(couple, "SSG"));
				break;
			case "CODICE-FUNZIONALITA":
				sb.append(createField(couple, "VIDPERIZIA"));
				break;
			case "TIPO-NOTIFICA":
				sb.append(createField(couple, "I"));
				break;
			case "DURATA":
				sb.append(createField(couple, "1"));
				break;
			case "CODICE-NODO":
				sb.append(createField(couple, ""));
				break;
			case "CODICE-COLLABORATORE":
				sb.append(createField(couple, ""));
				break;
			case "CODICE-ASSOCIAZIONE":
				sb.append(createField(couple, ""));
				break;
			case "DATA-INSERIMENTO":
				sb.append(createField(couple, "2018-10-29"));
				break;
			case "MESSAGGIO":
				sb.append(createField(couple, "Messaggio di test"));
				break;
			case "CHIAVE-0":
				sb.append(createField(couple, "CODSOGG"));
				break;
			case "VALORE-0":
				sb.append(createField(couple, "2346654"));
				break;
			case "CHIAVE-1":
				sb.append(createField(couple, "POLIZZA"));
				break;
			case "VALORE-1":
				sb.append(createField(couple, "370013783"));
				break;
			case "CHIAVE-2":
				sb.append(createField(couple, "CODCOMP"));
				break;
			case "VALORE-2":
				sb.append(createField(couple, "005"));
				break;
			case "CHIAVE-3":
				sb.append(createField(couple, "NODOSX"));
				break;
			case "VALORE-3":
				sb.append(createField(couple, "A78"));
				break;
			case "CHIAVE-4":
				sb.append(createField(couple, "ANNOSX"));
				break;
			case "VALORE-4":
				sb.append(createField(couple, "2018"));
				break;
			case "CHIAVE-5":
				sb.append(createField(couple, "SINISTRO"));
				break;
			case "VALORE-5":
				sb.append(createField(couple, "013455105"));
				break;
			default:
				sb.append(createField(couple, ""));
				break;
			}
		}
		return sb.toString();
	}

	private String createMessageASV() {
		StringBuilder sb = new StringBuilder();
		for (Couple<String, Integer> couple : list) {
			switch (couple.getKey()) {
			case "HEADER-ABI":
				sb.append(createField(couple, ""));
				break;
			case "ID-NOTIFICA":
				sb.append(createField(couple, "LF1H7C2QV"));
				break;
			case "SISTEMA-PROVENIENZA":
				sb.append(createField(couple, "ASV"));
				break;
			case "CODICE-FUNZIONALITA":
				sb.append(createField(couple, "ASV"));
				break;
			case "TIPO-NOTIFICA":
				sb.append(createField(couple, "I"));
				break;
			case "DURATA":
				sb.append(createField(couple, "7"));
				break;
			case "CODICE-NODO":
				sb.append(createField(couple, ""));
				break;
			case "CODICE-COLLABORATORE":
				sb.append(createField(couple, ""));
				break;
			case "CODICE-ASSOCIAZIONE":
				sb.append(createField(couple, ""));
				break;
			case "DATA-INSERIMENTO":
				sb.append(createField(couple, "2018-10-29"));
				break;
			case "MESSAGGIO":
				sb.append(createField(couple, "Richiesta affiancamento"));
				break;
			case "CHIAVE-0":
				sb.append(createField(couple, "UTENZA"));
				break;
			case "VALORE-0":
				sb.append(createField(couple, "busetto@058.gen"));
				break;
			case "CHIAVE-1":
				sb.append(createField(couple, "COD_EVENTO"));
				break;
			case "VALORE-1":
				sb.append(createField(couple, "NT_APP_INV"));
				break;
			default:
				sb.append(createField(couple, ""));
				break;
			}
		}
		return sb.toString();
	}

	private String createField(Couple<String, Integer> couple, String value) {
		return StringUtils.rightPad(StringUtils.substring(value, 0, couple.getValue()), couple.getValue(), ' ');
	}

}
